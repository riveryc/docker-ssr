FROM golang:1.12 AS builder
RUN go get github.com/shadowsocks/shadowsocks-go/cmd/shadowsocks-server

FROM ubuntu:latest
WORKDIR /bin
COPY --from=builder /go/bin/shadowsocks-server /bin/.
#COPY . .
RUN chmod +x /bin/shadowsocks-server /entrypoint.sh
#ENTRYPOINT [ "/entrypoint.sh" ]